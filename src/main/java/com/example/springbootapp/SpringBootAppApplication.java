package com.example.springbootapp;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SpringBootAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootAppApplication.class, args);
    }
}

@Slf4j
@RestController
class ListStoreController {

    private final Logger LOG = LoggerFactory.getLogger(ListStoreController.class);
    private List<String> store = new ArrayList<>();

    @GetMapping("/store")
    public List<String> retrieveData() {
        LOG.info("GET request sent to stores");
        return store;
    }

    @PostMapping("/store")
    public void storeData(@RequestBody String data) {
        LOG.info("POST request sent to stores");
        store.add(data);
        LOG.info("New store uploaded");
    }

    @PutMapping("/store/{index}")
    public void updateData(@PathVariable int index, @RequestBody String data) {
        LOG.info("PUT request sent to stores with id:{}", index);
        store.set(index, data);
    }

    @DeleteMapping("/store/{index}")
    public void deleteData(@PathVariable int index) {
        LOG.info("DELETE request sent to stores with id:{}", index);
        store.remove(index);
    }
}
